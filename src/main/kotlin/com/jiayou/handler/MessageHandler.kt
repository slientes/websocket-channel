package com.jiayou.handler

import com.jiayou.channel.OnRecive
import com.jiayou.channel.OnSend
import org.springframework.stereotype.Component

@Component
interface MessageHandler<T> {
    fun doWork(onReceive: OnRecive): OnSend
}