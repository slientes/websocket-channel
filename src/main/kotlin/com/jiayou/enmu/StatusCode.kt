package com.jiayou.enmu

/***
 * 返回结果状态码
 */

/***
 * 连接成功 Connection_Success
 * 调用成功 Success
 * 调用失败（无该方法）   Fail
 * 调用失败（onRec数据绑定异常）    error
 * 未知异常 Unknow
 */
enum class StatusCode {
    Connection_Success, Success,NoHandler, Fail, Error, Unknow
}