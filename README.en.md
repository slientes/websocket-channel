# 替代传统api的轻量级框架websocket-channel

    

本项目基于springboot，抛弃原有的api接口化约束,全程利用定制化websocket通信，项目基于spring-boot-starter-websocket再封装，适用于实时业务交互，即时游戏等，该框架对其做了非常轻量级的封装。



   

###  传统springmvc架构

![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/181807_161259d8_1682497.jpeg "在这里输入图片标题")

### Websocket-Channel架构

![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/181743_b27f37f9_1682497.jpeg "WebSocket架构")

### 项目基于kotlin1.3.61，jdk8

### 如何使用
 **引入** starter由于未上传maven中心仓库，请点击链接下载
[websocket-channel-spring-boot-starter](http://www.jiayou.art/websocket-channel-spring-boot-starter-1.0-Beta.jar)
```
        //在maven引入
        <dependency>
            <groupId>com.jiayou</groupId>
            <artifactId>websocket-channel-spring-boot-starter</artifactId>
            <version>1.0-Beta</version>
        </dependency>
```
 **yml配置** ，该配置是扫描到你的实际业务类，该类由@SocketBusiness注解修饰

```
socket:
  packages: com.jiayou   //扫描业务类
```

所有业务类需要注入IOC容器并且类由@SocketBusiness注解修饰，其方法由@SocketWork注解修饰


@SocketBusiness相当于类级别的@RequestMapping


@SocketWork  相当于方法级别的@RequestMapping

 **举个例子** ：
```
data class User(var id: Int, var name: String)

@Component
@SocketBusiness("人员")          //@RequstMapping('/人员')
class SelectService {

    @SocketWork("查询")          //@RequstMapping('/查询')
    fun select(parameterData: ParameterData): User {
        println(parameterData)
        return User(1, "websocket-channel")
    }

    @SocketWork("插入")          //@RequstMapping('/插入')
    fun insert(parameterData: ParameterData) {
        println("插入数据成功！")
    }
}
```
对应下来就是：


springmvc:


http://localhost:8080/人员/查询

http://localhost:8080/人员/插入

websocket-channel:
因为是socket通信，我们是用的是json格式的数据进行交互，那么它封装下来是这样的


```
{
  "id": "8ec52519-6d69-491e-be34-64b7c89f0826",        //前端随机生成的UUID，用于服务端将结果与UUID封装在一起前端判断是哪一个结果
  "from": "userID",                                    //用户的ID，可以用户自定义
  "intent": "人员-查询",                                //这就是我们映射的业务路径
  "parameterType": "Json",                             //暂时只能穿Json的数据，后续会更新
  "data": 2                                            //我们传的参数
}
```

 **SpringBoot监听** ：webscoket-channel需要扫描IOC容器中带有@SocketBusiness的Bean，配置如下



```
@SpringBootApplication
class DemoApplication : ApplicationListener<ContextRefreshedEvent> {

    @Bean
    fun serverEndpointExporter() = ServerEndpointExporter()

    @Autowired
    private lateinit var webSocketAutoConfig: WebSocketAutoConfig

    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        event.applicationContext.getBeanNamesForAnnotation(SocketBusiness::class.java).forEach {
            webSocketAutoConfig.scan(event.applicationContext.getBean(it).javaClass)
        }
        WebSocketEndpoint.configurableApplicationContext = event.applicationContext as WebApplicationContext
        println(WebSocketAutoConfig.endpointMap)
    }

}
```

 **WebSocket端点配置** ：端点类需要继承WebSocketEndpoint类，并注入IOC容器，且由@ServerEndpoint("/xx/xx/{id}")注解修饰




```
@Component
@ServerEndpoint("/server/{id}")
class Socket : WebSocketEndpoint()
```


### 验证

配置无误的话，你会看到控制台会打印这样一条日志：


```
com.jiayou.WebSocketEndpoint             : Global websocket Endpoint have bean initialized
```

 **且** 会输出你的业务类信息：



```
{人员={class com.example.demo.SelectService=[{插入=insert}, {查询=select}]}}
```
 **没有报错后我们进行下一步测试：** 


利用在线工具我们连接websocket-channel： ws://localhost:8080/server/123


连接成功后我们发送一条JSon：



```
{
  "id": "8ec52519-6d69-491e-be34-64b7c89f0826",
  "from": "123",
  "intent": "人员-查询",
  "parameterType": "Json",
  "data": "2"
}
```

服务端就会给你返回交互信息。


意见反馈交流：

![输入图片说明](http://jiayou.art/websocket-channel%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png "在这里输入图片标题")









